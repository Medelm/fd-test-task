﻿using BusinessLogicLayer.SubSystems;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitOfWorkLayer;
using UnitOfWorkLayer.Repository;
using Entities.Entities.DAL;

namespace CustomNinjectModule
{
    public class NinjectConfigurationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>();
            Bind<DbContext>().To<DataBase>();
            Bind<IVerificationSubSystem>().To<VerificationSubSystem>();
            Bind<IGenericRepository<User>>().To<IGenericRepository<User>>();
            Bind<IGenericRepository<Admin>>().To<IGenericRepository<Admin>>();
            Bind<IGenericRepository<Admin>>().To<IGenericRepository<Admin>>();
            Bind<IGenericRepository<Recrutier>>().To<IGenericRepository<Recrutier>>();
        }
    }
}
