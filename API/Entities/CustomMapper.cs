﻿using AutoMapper;
using Entities.Entities.BLL;
using Entities.Entities.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Entities
{
    public static class CustomMapper
    {
        static IMapper iMapper;
        static CustomMapper()
        {
            var config = new MapperConfiguration(cfg => {

                cfg.CreateMap<User, UserBLL>();
                cfg.CreateMap<Recrutier, RecrutierBLL>();
                cfg.CreateMap<Admin, AdminBLL>();
            });
            iMapper = config.CreateMapper();
           

        }
        public static UserBLL getUserBLL(User sc)
        {
            return iMapper.Map<UserBLL>(sc);
        }
        public static RecrutierBLL getRecruiterBLL(Recrutier sc)
        {
            return iMapper.Map<RecrutierBLL>(sc);
        }
        public static AdminBLL getAdminBLL(Admin sc)
        {
            return iMapper.Map<AdminBLL>(sc);
        }

    }
}