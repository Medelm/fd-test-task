﻿
using BusinessLogicLayer;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.SubSystems;
using CustomNinjectModule;
using Entities;
using Entities.Entities.BLL;
using Entities.Entities.DAL;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace WebApiService.Controllers
{
    public class UserController : ApiController
    {
        IKernel kernel;
        SystemFacade database;
      
        public UserController()
        {
            kernel = new StandardKernel(new NinjectConfigurationModule());
            database = new SystemFacade(kernel.Get<IVerificationSubSystem>());

        }
        // Verification of user.
        [Route("api/user/{login}/{password}")]
        public Object Get(string login, string password)
        {
            return database.checkUser(login, password);
        }

    }
}
