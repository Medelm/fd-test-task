﻿namespace DataAccessLayer
{
    using Entities.Entities.DAL;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class DataBase : DbContext
    {
       

        public DataBase()
            : base("name=MyModel")
        {
            Database.SetInitializer(new CustomDbInitiaslizer());
            //Configuration.LazyLoadingEnabled = false;
        }
        public virtual DbSet<User> users { get; set; }
        public virtual DbSet<Recrutier> recruters { get; set; }
        public virtual DbSet<Admin> admins { get; set; }
        public class CustomDbInitiaslizer : DropCreateDatabaseAlways<DataBase>
        {
            string text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam molestie augue, gravida lobortis metus luctus eu.";
            protected override void Seed(DataBase context)
            {
                context.users.Add(new User() { Login = "Slink", Password = "1" });
                context.users.Add(new User() { Login = "User1", Password = "2" });
                context.users.Add(new User() { Login = "User2", Password = "2" });
                context.users.Add(new User() { Login = "User3", Password = "2" });
                context.users.Add(new User() { Login = "User4", Password = "2" });
                context.users.Add(new User() { Login = "User5", Password = "2" });
                context.users.Add(new User() { Login = "User6", Password = "2" });
                context.users.Add(new User() { Login = "User7", Password = "2" });
                context.users.Add(new User() { Login = "User8", Password = "2" });
                context.users.Add(new User() { Login = "User9", Password = "2" });
                context.users.Add(new User() { Login = "User10", Password = "2" });

                context.recruters.Add(new Recrutier() { Login = "Recr", Password = "12345" });
                context.recruters.Add(new Recrutier() { Login = "Recr1", Password = "1" });
                context.recruters.Add(new Recrutier() { Login = "Recr2", Password = "2" });
                context.recruters.Add(new Recrutier() { Login = "Recr3", Password = "3" });

                context.admins.Add(new Admin() { Login = "Admin", Password = "1" });

                context.SaveChanges();
            }
        }
    }

  
}