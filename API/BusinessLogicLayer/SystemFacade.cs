﻿using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.SubSystems;
using Entities.Entities.BLL;
using Entities.Entities.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer
{
    
   
    public class SystemFacade
    {
        IVerificationSubSystem verificationSystem;
        public SystemFacade(IVerificationSubSystem verificationSystem)
        {
            this.verificationSystem = verificationSystem;
        }
        public object checkUser(string login, string password)
        {
            return verificationSystem.CheckUser(login, password);
        }
    }
}