﻿Feature: LogIn_Feature
 In order to access my account
As a user of the website
I want to log into the website
 
@mytag
Scenario Outline: Successful Login with Valid Credentials
 Given Home Page is opened
 And I navigate to LogIn Page
 When I enter <username> and <password>
 And I Log In
 Then Successful LogIN message is displayed
Examples:
| username   | password |
| testuser_1 | Test@123 |
| testuser_2 | Test@153 |
