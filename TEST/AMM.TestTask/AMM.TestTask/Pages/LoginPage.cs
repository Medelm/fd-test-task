﻿using OpenQA.Selenium;

namespace AMM.TestTask.Pages
{
    class LoginPage
    {
        public IWebElement NameInput => WebBrowser.WebDriver.FindElement(By.Id("username"));
        public IWebElement PasswordInput => WebBrowser.WebDriver.FindElement(By.Id("password"));
        public IWebElement LoginButton => WebBrowser.WebDriver.FindElement(By.Name("login"));
    }
}