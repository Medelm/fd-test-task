﻿using OpenQA.Selenium;

namespace AMM.TestTask.Pages
{
    class HomePage
    {
        public IWebElement DismissButton => WebBrowser.WebDriver.FindElement(By.XPath("//a[contains(.,'Dismiss')]"));
        public IWebElement AccountButton => WebBrowser.WebDriver.FindElement(By.XPath("//a[contains(.,'My Account')]"));
    }
}