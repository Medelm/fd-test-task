﻿using System;
using System.IO;
using OpenQA.Selenium;

namespace AMM.TestTask
{
    public class WebBrowser : IDisposable
    {
        public static IWebDriver WebDriver { get; set; }
        public static string BaseUrl { get; set; }
        public static string DataSource { get; set; }
        public static string InitialCatalog { get; set; }


        public WebBrowser()
        {
            WebDriver = new CustomChromeDriver();
            Directory.SetCurrentDirectory(AppContext.BaseDirectory.Substring(0,
                AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal)));
        }

        public static void TakeScreenshot(string screenshotFilePath)
        {
            if (WebDriver is ITakesScreenshot takesScreenshot)
            {
                var screenshot = takesScreenshot.GetScreenshot();
                screenshot.SaveAsFile(screenshotFilePath);
                Console.WriteLine("Screenshot: {0}", new Uri(screenshotFilePath));
            }
        }

        public static INavigation Navigate()
        {
            return WebDriver.Navigate();
        }

        public static void RefreshPage()
        {
            WebDriver.Navigate().Refresh();
        }

        public static void GoToWithBaseUrl(string url)
        {
            WebDriver.Navigate().GoToUrl(BaseUrl + url);
        }

        public static object ExecuteJsOnPage(string js)
        {
            if (!(WebDriver is IJavaScriptExecutor javaScriptExecutor))
            {
                throw new InvalidOperationException("WebDriver doesn't support executing of JS.");
            }

            return javaScriptExecutor.ExecuteScript(js);
        }

        public void Dispose()
        {
            Console.WriteLine("Dispose webdriver.");
            WebDriver.Quit();
        }
    }
}
