﻿using System;
using OpenQA.Selenium.Chrome;

namespace AMM.TestTask
{
    public class CustomChromeDriver : ChromeDriver
    {
        private static readonly Func<ChromeDriverService> CustomDriverService = () =>
        {
            var service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            return service;
        };

        private static readonly Func<ChromeOptions> CustomOptions = () =>
        {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--no-sandbox");
            chromeOptions.AddArgument("--disable-gpu");
            chromeOptions.AddArgument("--start-maximized");
            chromeOptions.AddArgument("--ignore-certificate-errors");
            chromeOptions.AddArgument("disable-extensions");
            chromeOptions.AddArguments("test-type");
            return chromeOptions;
        };

        public CustomChromeDriver()
            : base(CustomDriverService(), CustomOptions())
        {
        }
    }
}
