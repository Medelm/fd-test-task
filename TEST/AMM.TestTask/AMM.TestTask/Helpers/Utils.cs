﻿using System.IO;
using System.Text;

namespace AMM.TestTask.Helpers
{
    public static class Utils
    {
        public static string ToPath(string s)
        {
            var builder = new StringBuilder(s);
            foreach (var invalidChar in Path.GetInvalidFileNameChars())
            {
                builder.Replace(invalidChar.ToString(), "");
            }
            builder.Replace(' ', '_');
            return builder.ToString();
        }
    }
}
