﻿using AMM.TestTask.Pages;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace AMM.TestTask.Steps
{
    [Binding]
    public class LoginSteps
    {
        private LoginPage _loginPage;
        private HomePage _homePage;

        [Given(@"Home Page is opened")]
        public void GivenUserIsAtTheHomePage()
        {
            _homePage = new HomePage();
            _homePage.DismissButton.Click();
        }

        [Given(@"I navigate to LogIn Page")]
        public void GivenNavigateToLogInPage()
        {
            _homePage.AccountButton.Click();
            _loginPage = new LoginPage();
        }

        [When(@"I enter (.*) and (.*)")]
        public void WhenUserEnterAnd(string username, string password)
        {
            _loginPage.NameInput.SendKeys(username);
            _loginPage.PasswordInput.SendKeys(password);
        }

        [When(@"I Log In")]
        public void WhenClickOnTheLogInButton()
        {
            _loginPage.LoginButton.Click();
        }

        [Then(@"Successful LogIN message is displayed")]
        public void ThenSuccessfulLogInMessageIsDisplayed()
        {
            true.Equals(WebBrowser.WebDriver.FindElement(By.XPath(".//*[@id='account_logout']/a")).Displayed);
        }
    }
}