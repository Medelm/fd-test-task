﻿using System;
using System.IO;
using TechTalk.SpecFlow;
using System.Configuration;
using AMM.TestTask.Helpers;

namespace AMM.TestTask.Bindings
{
    [Binding]
    public sealed class GlobalBindings
    {
        private WebBrowser _fixture;

        [BeforeScenario]
        public void PrepareForTest()
        {
            Directory.SetCurrentDirectory(AppContext.BaseDirectory.Substring(0,
                AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal)));
            _fixture = new WebBrowser();
            WebBrowser.BaseUrl = ConfigurationManager.AppSettings["BaseUrl"];
            WebBrowser.DataSource = ConfigurationManager.AppSettings["DataSource"];
            WebBrowser.InitialCatalog = ConfigurationManager.AppSettings["InitialCatalog"];
            WebBrowser.Navigate().GoToUrl(WebBrowser.BaseUrl);
        }

        [AfterScenario]
        public void Cleanup()
        {
            try
            {

                if (ScenarioContext.Current.TestError != null)
                {
                    // get and save a screenshot from a webdriver
                    var screenshotFileName = Utils.ToPath(
                        $"{FeatureContext.Current.FeatureInfo.Title}_{ScenarioContext.Current.ScenarioInfo.Title}_{DateTime.Now:s}.png");
                    var screenshotFilePath = TestFolders.GetOutputFilePath(screenshotFileName);
                    WebBrowser.TakeScreenshot(screenshotFilePath);
                }
            }
            finally
            {
                _fixture.Dispose();
            }
        }
    }
}
